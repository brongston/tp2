﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;

public class Lvlgenerator : MonoBehaviour
{
    [SerializeField] private Transform m_player = null;
    [SerializeField] private List<GameObject> m_Plateformlist = null;
    [SerializeField] private int m_StartingPlatformCount = 3;
    [SerializeField] private GameObject m_PlateformStart = null;
    [SerializeField] private int m_MaxPlatform = 5;
    

    private Transform m_lastPlatform;
    private List<Transform> m_PlatformCache = null;
    private void Awake()
    {
        m_PlatformCache = new List<Transform>();
        m_lastPlatform = m_PlateformStart.transform;

        for (int i = 0; i < m_StartingPlatformCount; i++)
        {
            Spawn();
        }

    }
    private void Update()
    {
        if (Vector3.Distance(m_player.position, m_lastPlatform.position) < 30.0f)
        {
            Spawn();
        }
    }
    private void Spawn()
    {
        Transform flag = m_lastPlatform.transform.Find("flag");
        m_lastPlatform = SpawnPlateform(flag.position);
        m_PlatformCache.Add(m_lastPlatform);

        if (m_MaxPlatform < m_PlatformCache.Count )
        {
            Transform toDelete = m_PlatformCache[0];
            Destroy(toDelete.gameObject);
            m_PlatformCache.RemoveAt(0);
        }
    }
    private Transform SpawnPlateform(Vector3 spawnPosition)
    {
        int index4 = UnityEngine.Random.Range(0, m_Plateformlist.Count);
        GameObject lastplatform = Instantiate(m_Plateformlist[index4], spawnPosition, Quaternion.identity);
        return lastplatform.transform;
    }

}