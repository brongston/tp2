﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    [SerializeField] private Animator m_Animator = null;
    [SerializeField] private GameObject Goblin = null;
    private Rigidbody2D m_GobRigidbody;
    // Start is called before the first frame update
    void Start()
    {
        m_GobRigidbody = GetComponent<Rigidbody2D>();
        gameObject.layer = LayerMask.NameToLayer("Eye");
    
    }

    // Update is called once per frame
    void Update()
    {
        OnCollisionEnter();
    }



    public void OnCollisionEnter()
    {
        
        if (m_GobRigidbody.IsTouchingLayers(LayerMask.GetMask("IsStriking"))|| m_GobRigidbody.IsTouchingLayers(LayerMask.GetMask("Invincible")))
        {
            
            m_Animator.SetBool("Ishit", true);
            Goblin.layer = LayerMask.NameToLayer("Water");
        }
    }

   
}
