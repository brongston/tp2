﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Player_controller player_Controller = collider.gameObject.GetComponent<Player_controller>();
        if (player_Controller != null)
        {
            player_Controller.Shield();
            Destroy(gameObject);
        }


    }
}
