﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionMenuUI : MonoBehaviour
{
    [SerializeField] private Toggle m_FullscreenToggle = null;
    [SerializeField] private Dropdown m_Resolution = null;
    private void Start()
    {
        m_FullscreenToggle.isOn = Screen.fullScreen;

        List<string> resolution = new List<string>();
        int currentResolution = 0;

        foreach (Resolution res in Screen.resolutions)
        {
            string item = res.width + "x" + res.height;
            resolution.Add(item);
        if(PlayerPrefs.HasKey("ResW")&& PlayerPrefs.HasKey("ResH"))
            if (res.width == PlayerPrefs.GetInt("ResW") && res.height == PlayerPrefs.GetInt("ResH"))
            {
                currentResolution = resolution.Count - 1;
            }
        }

        m_Resolution.AddOptions(resolution);
        m_Resolution.value = currentResolution;
        m_Resolution.RefreshShownValue();
    }
    public void ResolutionChanged(int index)
    {
        Debug.Log("index is:" + index);
        Resolution res = Screen.resolutions[index];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);

        PlayerPrefs.SetInt("ResW", res.width);
        PlayerPrefs.SetInt("ResH", res.height);
        PlayerPrefs.Save();

    }
    public void Toggle(bool value)
    {
      Screen.fullScreen = value;    
    }  
}
