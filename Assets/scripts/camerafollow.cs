﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camerafollow : MonoBehaviour
{
    [SerializeField] public bool m_TouchSkull = false;
    [SerializeField] private Transform m_target = null;
    

    void Start()
    {
        Vector3 position = transform.position;
        position.z = m_target.position.z - 9;
        transform.position = position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position.x = m_target.position.x + 15;
        
        position.y = m_target.position.y +3;

        transform.position = position;
    }

    public void Skull()
    {
        // https://answers.unity.com/questions/20337/flipmirror-camera.html

        Matrix4x4 mat = Camera.main.projectionMatrix;
        mat *= Matrix4x4.Scale(new Vector3(-1, 1, 1));
        Camera.main.projectionMatrix = mat;
    }
}
