﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UICAM : MonoBehaviour
{
    
    [SerializeField] private Player_controller m_Player = null;
    [SerializeField] private Text m_GemText = null;
    [SerializeField] private Text m_GemText2 = null;   
    [SerializeField] private Text m_ScoreText = null;
    [SerializeField] private Text m_ScoreText2 = null;
    [SerializeField] private Text m_HighScoretext = null;
    private float m_HighScored;
   
    // Start is called before the first frame update


    void Start()
    {
        m_Player.m_Gold = PlayerPrefs.GetInt("Gems_count");
        m_HighScored = PlayerPrefs.GetInt("HighScore");
        Debug.Log(m_HighScored);
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("Main menu");   
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("lvl1");
    }
    // Update is called once per frame
    public void GiveButtonPressed()
    {
        m_Player.GiveGold(5);
    }

    public void SetGemText(int gems)
    {
        m_GemText.text = gems.ToString();
        m_GemText2.text = gems.ToString();
        PlayerPrefs.SetInt("Gems_count", m_Player.m_Gold);
        PlayerPrefs.Save();
    }

   

    public void SetScore(float Score) 
    {

        m_ScoreText.text = Score.ToString("00000");
        m_ScoreText2.text = Score.ToString("00000");
       

        PlayerPrefs.SetInt("score", (int)m_Player.m_Score);

        

    }
    public void ResetGem(int gems)
    {
        m_GemText.text = gems.ToString();
        m_GemText2.text = gems.ToString();
        PlayerPrefs.SetInt("Gems_count", m_Player.m_Gold*0);
        PlayerPrefs.Save();
    }

    public void SetHighScore()
    {
        Debug.Log("This is a bleuet");
        if (m_Player.m_Score > m_HighScored)
        {
            m_HighScored = m_Player.m_Score;

        }
        m_HighScoretext.text = m_HighScored.ToString("00000");
        PlayerPrefs.SetInt("HighScore", (int)m_HighScored);
        PlayerPrefs.Save();

    }
}
