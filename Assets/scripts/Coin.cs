﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private GameObject m_Coin;
   // private Rigidbody2D m_CoinRigidbody;
    void Start()
    {
        gameObject.layer = LayerMask.NameToLayer("Coin");
       // m_CoinRigidbody = GetComponent<Rigidbody2D>();
        m_Coin = gameObject;
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Player_controller player_Controller = collider.gameObject.GetComponent<Player_controller>();
        if (player_Controller != null)
        {
            Debug.Log("Muda");
            player_Controller.GiveGold(5);
            Destroy(m_Coin);
        }
    }
}
