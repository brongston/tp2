﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike_Death : MonoBehaviour
{
    [SerializeField] private Transform m_target = null;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.layer = LayerMask.NameToLayer("Eye");
        Vector3 position = transform.position;
        position.y = m_target.position.y - 25;
        transform.position = position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position.x = m_target.position.x + 5;
       

        transform.position = position;
    }
}
