﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Skull : MonoBehaviour
{
   
    [SerializeField]  private camerafollow m_camera = null;


    public void OnTriggerEnter2D(Collider2D collider)
    {
        Player_controller player_Controller = collider.gameObject.GetComponent<Player_controller>();
        if (player_Controller != null)
        {
            m_camera.Skull();
            Destroy(gameObject);
        }


    }
    
}
