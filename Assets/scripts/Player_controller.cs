﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class Player_controller : MonoBehaviour
{
     private bool m_IsAlive;
     private bool m_ShieldOn = false;
    [SerializeField] private Transform m_target = null; 
     private float m_startingSpeed = 15.0f;
     private float m_JumpSpeed = 20.0f;
     private int m_JumpCount = 0;
     public float m_Score = 0;
    private float m_Boost = 0.0f;
    [SerializeField] private GameObject m_GameOver = null;
    [SerializeField] private GameObject m_UIN = null;
    [SerializeField] private GameObject m_Playershield = null;
    [SerializeField] private Animator m_Animator = null;
    public int m_Gold = 0;    
    [SerializeField] private UICAM m_Hud = null;
    private Rigidbody2D m_rigidbody;
    [SerializeField] private AudioSource AudioSource = null;
    [SerializeField] private AudioClip Slash = null;
    [SerializeField] private AudioClip JumpSound = null;
    [SerializeField] private AudioClip DashSound = null;
    private float m_currentSpeed;
    void Start()
    {       
        m_Animator = GetComponentInChildren<Animator>();
        m_currentSpeed = m_startingSpeed;
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_IsAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Being alive is a good trick to survive more
        if (m_IsAlive)
        {
            Score();
            DropKick();           
            TouchingGround();
            Running();
            Jumping();
            Rolling();
            Striking();
            m_Boost /= 1 + Time.deltaTime;
            m_currentSpeed = m_currentSpeed + (0.1f * Time.deltaTime);
        }
    }



    private void Rolling()
    {
        bool Mousedown = Input.GetKeyDown(KeyCode.LeftShift);
        if (Mousedown && m_Animator.GetBool("NotRolling") == false && (m_Animator.GetBool("IsHit") == false))
        {
            AudioSource.PlayOneShot(DashSound);
            //Roll work with boost
            gameObject.layer = LayerMask.NameToLayer("Rolling");
            m_Boost = 15.0f;
            m_Animator.Play("HeroKnight_Roll");
            m_Animator.SetBool("NotRolling", true);
            return;
        }

        if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            m_Animator.SetBool("NotRolling", false);           
            gameObject.layer = LayerMask.NameToLayer("Default");                    
        }
    }


    private void Running()
    {
        Vector2 playerVelocity = new Vector2(m_currentSpeed + m_Boost, m_rigidbody.velocity.y);
        m_rigidbody.velocity = playerVelocity;//Boost from role+ velocity for the jump
        //Running in the 90's
    }

    private void Jumping()
    {
        if (m_Animator.GetBool("IsJumping") == false || m_JumpCount < 1 
            || m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Ground")) 
            || m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Eye")))
        {
            //Jump around, JUMP, JUMP, JUMP, JUMP, JUMP, JUMP, JUMP, JUMP,
            bool isJumping = Input.GetKeyDown(KeyCode.Space);           
            if (isJumping)
            {
                if (m_ShieldOn == false)
                {
                    gameObject.layer = LayerMask.NameToLayer("Default");
                }
                AudioSource.PlayOneShot(JumpSound);
                Vector2 jumpingVelocity = new Vector2(0.0f, m_JumpSpeed);
                m_rigidbody.velocity += jumpingVelocity;
                m_Animator.SetBool("IsJumping", true);
                m_Animator.SetBool("playerDropKick", false);
                m_JumpCount = m_JumpCount + 1;
                return;
            }
            if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
            {
                m_Animator.SetBool("IsJumping", false);                            
                gameObject.layer = LayerMask.NameToLayer("Default");
                
            }
        }
    }

    private void Striking()
    {
        //invincibiliter pour un temps
      
        bool Mousedown = Input.GetKeyDown(KeyCode.C);
            if (Mousedown && m_Animator.GetBool("Mousedown") == false && (m_Animator.GetBool("IsHit") == false))
            {
                 m_Boost = 2.0f;
                gameObject.layer = LayerMask.NameToLayer("IsStriking");
                m_Animator.SetBool("Mousedown", true);
                AudioSource.PlayOneShot(Slash);
                return;
            }
        
        if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 && (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Strike")))
        {
            Debug.Log("YoAngello");
            m_Animator.SetBool("Mousedown", false);
            gameObject.layer = LayerMask.NameToLayer("Default");

        }     
    }


    private void TouchingGround()
    {
        if (m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            //reset pour double jump
            Debug.Log("ground");
            m_JumpCount = 0;
            m_Animator.SetBool("playerDropKick", false);
        }
    }   
    public void OnCollisionEnter2D(Collision2D collision)
    {
        //thankfull death
        Debug.Log(LayerMask.NameToLayer("Eye"));
        if (collision.gameObject.layer == 13 && m_ShieldOn == false && (gameObject.layer!= 12))
        {
            Debug.Log("OOF1");
            m_IsAlive = false;
            m_Animator.SetBool("IsJumping", false);
            m_Animator.SetBool("IsHit", true);
            m_Boost = m_Boost * 0;
            m_currentSpeed = m_currentSpeed * 0;
            m_Animator.SetBool("Mousedown", false);
            gameObject.layer = LayerMask.NameToLayer("IsDead");
            m_rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            m_JumpSpeed = m_JumpSpeed * 0;
            StartCoroutine(GameOver());                        
        }
    }
    private IEnumerator GameOver()//get good
    {
        while (!(m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 &&
        (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("HeroKnight_DeathNoBlood"))))
        {
            yield return null;
        }
        m_Hud.SetHighScore();
        m_GameOver.SetActive(true);
        m_UIN.SetActive(false);
    }

    public void GiveGold(int gold)
    {
        Debug.Log("notified from UI in player");
        m_Gold += gold;
        m_Hud.SetGemText(m_Gold);//Hud = UIcam
    }

   



    private void DropKick()// David est vraiment meilleur que David
    {
        if (m_Animator.GetBool("playerDropKick") == false && (!m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Ground"))))
        {

            bool isDK= Input.GetKeyDown(KeyCode.S);
            if (isDK)
            {
                gameObject.layer = LayerMask.NameToLayer("Default");
                Vector2 DKVelocity = new Vector2(0.0f, -m_JumpSpeed);
                m_rigidbody.velocity =DKVelocity;
                m_Animator.SetBool("playerDropKick", true);
                m_Animator.SetBool("IsJumping", false);
               
                Debug.Log("OOF3");
                return;
            }

            if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 
                || m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Eye"))
                || m_rigidbody.IsTouchingLayers(LayerMask.GetMask("Ground")))
            {
                m_Animator.SetBool("playerDropKick", false);
                gameObject.layer = LayerMask.NameToLayer("Default");
                
            }

        }
    }


    public void Score()//Player score
    {
        Vector3 position = transform.position;
        m_Score = m_target.position.x;
        m_Hud.SetScore(m_Score);
        
    }

    public void Shield()//Boost de shield 
    {
        m_ShieldOn = true;
       m_Animator.SetBool("Shield", true);     
       gameObject.layer = 18;
        StartCoroutine(Shieldtimer());     
    }

    private IEnumerator Shieldtimer()
    {        
            yield return new WaitForSeconds(3);        
        m_Animator.SetBool("Shield", false);
        m_ShieldOn = false;
        m_Playershield.layer = 0;
    }

}